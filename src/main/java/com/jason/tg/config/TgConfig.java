package com.jason.tg.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "tg")
public class TgConfig {
    private String botName;
    private String botToken;
    private Long delay;
    private Boolean proxy;
    private String proxyHost;
    private Integer proxyPort;
    private String webhookUrl;
    private String serviceUrl;
}
