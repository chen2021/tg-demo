package com.jason.tg;

import com.jason.tg.service.ChatBot;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.bots.TelegramWebhookBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.generics.WebhookBot;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@SpringBootTest
public class TgTest {
    @Autowired
    ChatBot chatBot;
    @Test
    void hello(){
        System.out.println("Hello world!");
    }

    @Test
    void shareMsg(){
        chatBot.sendShareMessage("1860002311","hello");

    }
}
