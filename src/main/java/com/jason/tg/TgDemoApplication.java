package com.jason.tg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TgDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(TgDemoApplication.class, args);
    }

}
