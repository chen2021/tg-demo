package com.jason.tg.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("ops")
public class OpsController {
    @GetMapping("ping")
    public String ping() {
        return "pong----v1";
    }
}
