package com.jason.tg.config;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.jason.tg.enums.ECCEnum;
import com.jason.tg.service.ChatBot;
import com.jason.tg.util.BASE64Encoder;
import com.jason.tg.util.ECCUtil;
import com.jason.tg.util.TokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.updates.SetWebhook;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor
public class TelegramRunner implements ApplicationRunner {
    private final ChatBot chatBot;
    private final RestTemplate template;

    private final TgConfig tgConfig;

//    private final StringRedisTemplate stringRedisTemplate;

    @Override
    public void run(ApplicationArguments args) {
        try {
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
            botsApi.registerBot(chatBot, new SetWebhook());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Bean
    private void setWebhook() {
        try {
            ECCUtil.map = ECCUtil.getGenerateKey();
            HttpHeaders headers = new HttpHeaders();
            Map<String, String> params = new HashMap<>();
            String uuid = TokenUtil.getUuid();
//            //ECC加密
            String pubKey = ECCUtil.map.get(ECCEnum.PUBLIC_KEY.value());
            byte[] b = ECCUtil.encrypt(uuid.getBytes(), pubKey);
            String token = BASE64Encoder.encodeBuffer(b);

       /*     String url = tgConfig.getWebhookUrl() + "/tg/" + token;
            if (Boolean.TRUE.equals(stringRedisTemplate.hasKey("telegram:token:ecc:" + tgConfig.getBotName()))) {
                url = tgConfig.getWebhookUrl() + "/tg/" + stringRedisTemplate.opsForValue().get("telegram:token:ecc:" + tgConfig.getBotName());
            } else {
                stringRedisTemplate.opsForValue().set("telegram:token:ecc:" + tgConfig.getBotName(), token);
            }*/
            String url = tgConfig.getWebhookUrl() + "/tg";
            params.put("url", url);
            params.put("max_connections", 100 + "");
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> request = new HttpEntity<>(JSON.toJSONString(params), headers);
            Object o = template.postForObject(tgConfig.getServiceUrl() + "/bot" + tgConfig.getBotToken() + "/setWebhook", request, Object.class);
            System.out.println(o.toString());
        } catch (Exception e) {
            log.error("Telegram webhookUrl回调地址设置异常", e);
        }
    }
}
