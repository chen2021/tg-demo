package com.jason.tg.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultBotOptions;

@Slf4j
@Component
public class DefaultBotProxyOptions extends DefaultBotOptions {
    @Autowired
    public DefaultBotProxyOptions(TgConfig tgConfig) {
        if (tgConfig.getProxy()) {
            this.setProxyHost(tgConfig.getProxyHost());
            this.setProxyPort(tgConfig.getProxyPort());
            this.setProxyType(ProxyType.SOCKS5);
        }
    }

}
