package com.jason.tg.controller;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.telegram.telegrambots.meta.api.objects.Update;

@Slf4j
@RestController
@RequestMapping("tg")
public class TgController {
    @PostMapping("/{token}")
    public void tg(@PathVariable String token, @RequestBody Update update) {
        System.out.println(JSONUtil.toJsonPrettyStr(update));
    }
}
