package com.jason.tg.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayOutputStream;

@Slf4j
@Component
public class RestTemplateConfig {
    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(restTemplate.getRequestFactory()));
        restTemplate.getInterceptors().add((httpRequest, bytes, clientHttpRequestExecution) -> {
            Long start = System.currentTimeMillis();
            ClientHttpResponse response = clientHttpRequestExecution.execute(httpRequest, bytes);
            Long end = System.currentTimeMillis();
            try (ByteArrayOutputStream ops = new ByteArrayOutputStream()) {
                log.debug("Rest ~  耗时->【{}ms】 URI信息->【{}】 请求方式->【{}】 参数->【{}】 Headers->【{}】", end - start, httpRequest.getURI().toString(), httpRequest.getMethodValue(), new String(bytes), httpRequest.getHeaders());
            } catch (Exception e) {
                log.error("复制流异常", e);
            }
            return response;

        });
        return restTemplate;
    }
}
