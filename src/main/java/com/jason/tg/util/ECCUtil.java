package com.jason.tg.util;

import com.jason.tg.enums.ECCEnum;


import javax.crypto.Cipher;
import javax.crypto.NullCipher;
import java.io.Serializable;
import java.security.*;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

public class ECCUtil implements Serializable {

    public static Map<String,String> map = null;

    public static byte[] encrypt(byte[] data, String publicKey)
            throws Exception {
        byte[] keyBytes = BASE64Decoder.decodeBuffer(publicKey);

        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(ECCEnum.ALGORITHM.value());

        ECPublicKey pubKey = (ECPublicKey) keyFactory
                .generatePublic(x509KeySpec);

        Cipher cipher = new NullCipher();
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        return cipher.doFinal(data);
    }

    public static byte[] decrypt(byte[] data, String privateKey) throws Exception {
        byte[] keyBytes = BASE64Decoder.decodeBuffer(privateKey);

        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(ECCEnum.ALGORITHM.value());

        ECPrivateKey priKey = (ECPrivateKey) keyFactory
                .generatePrivate(pkcs8KeySpec);

        Cipher cipher = new NullCipher();
        cipher.init(Cipher.DECRYPT_MODE, priKey);

        return cipher.doFinal(data);
    }
    static {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    public static Map<String,String> getGenerateKey() throws NoSuchProviderException, NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ECCEnum.ALGORITHM.value(),
                ECCEnum.PROVIDER.value());
        keyPairGenerator.initialize(256, new SecureRandom());
        KeyPair kp = keyPairGenerator.generateKeyPair();
        ECPublicKey publicKey = (ECPublicKey) kp.getPublic();
        ECPrivateKey privateKey = (ECPrivateKey) kp.getPrivate();
        Map<String,String> map = new HashMap<>();

        map.put(ECCEnum.PRIVATE_KEY.value(), BASE64Encoder.encodeBuffer(privateKey.getEncoded()));
        map.put(ECCEnum.PUBLIC_KEY.value(), BASE64Encoder.encodeBuffer(publicKey.getEncoded()));
        return map;
    }


    public static void main(String[] args) {
//        // 测试文本
//        byte[] plain = "123".getBytes();
//
//        // 生成密钥对
//        KeyPair keyPair = generateECCKeyPair(256);
//        PublicKey publicKey = keyPair.getPublic();
//        PrivateKey privateKey = keyPair.getPrivate();
//
//        // 加解密
//        byte[] encrypt = eccEncrypt(publicKey, plain);
//        byte[] decrypt = eccDecrypt(privateKey, encrypt);
//        System.err.println(new String(decrypt).equals(new String(plain)));


        try {
            map = getGenerateKey();
            String privKey = map.get(ECCEnum.PRIVATE_KEY.value());
            String pubKey = map.get(ECCEnum.PUBLIC_KEY.value());

            System.out.println("私钥：" + privKey);

            System.out.println("公钥：" + pubKey);
            String text = "asdvcsa";
            byte [] b = ECCUtil.encrypt(text.getBytes(),pubKey);
            String str = BASE64Encoder.encodeBuffer(b);
            System.out.println("密文：" + str);
            String outputStr = new String(ECCUtil.decrypt(BASE64Decoder.decodeBuffer(str),privKey));
            System.out.println("原始文本：" + text);
            System.out.println("解密文本：" + outputStr);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}